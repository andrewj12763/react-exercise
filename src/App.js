import { useState } from 'react';
import './App.scss';
import InfoSelection from './components/InfoSelection';
import ListView from './components/ListView';

function App() {
  const [list, setList] = useState('Representative');
  const [listData, setListData] = useState([{name: "Ryan Nelson", party: "R"}]);

  const updateList = (data) => {

  }

  return (
    <div className="App">
      <div className='page-title'>Who's My Representative?</div>
      <InfoSelection updateList={updateList} />
      <ListView listView={list} listData={listData} />
    </div>
  );
}

export default App;
