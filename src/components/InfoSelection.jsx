import { useState } from "react";
import "../App.scss";
var reactUsStates = require("react-us-states")


const InfoSelection = ({updateList}) => {
    const [infoSelection, setInfoSelection] = useState({list: "", state: ""});
    const [stateList] = useState(reactUsStates.useStates().map(i => i.name));

    const onConfirm = (e) => {
        e.preventDefault();
        updateList(infoSelection);
    }
    return (
        <div className="info-container"> 
            <form onSubmit={(e) => onConfirm(e)}>
                <select required value={infoSelection.list} onChange={(e) => setInfoSelection({...infoSelection, list: e.target.value})}>
                    <option value="">Select Type</option>
                    <option value="Representative">Representative</option>
                    <option value="Senator">Senator</option>
                </select>
                <select required value={infoSelection.state} onChange={(e) => setInfoSelection({...infoSelection, state: e.target.value})}>
                    <option value="">Select State</option>
                    {(stateList && stateList.length) && stateList.map((state, i) => {
                        return (
                            <option key={i} value={state}>{state}</option>
                        )
                    })}
                </select>
                <button>Submit</button>
            </form>
        </div>
    )
}


export default InfoSelection;