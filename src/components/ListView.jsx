import { useState } from "react";

const ListView = ({listView, listData}) => {
    const [selected, setSelected] = useState({first_name: "First Name", 
                                              last_name: "Last Name",
                                              district: "District",
                                              phone: "Phone",
                                              office: "Office"
                                            });

    return (
        <div className="list-container">
            <div className="list">
                <div className="list-title">List / <span>{listView}</span></div>
                <div>
                    <div className="list-headers">
                        <div>Name</div>
                        <div>Party</div>
                    </div>
                    <div>
                        {(listData && listData.length) ? listData.map((item, i) => {
                            return (
                            <div className="list-items" key={i}>
                                <div>{item.name}</div>
                                <div>{item.party}</div>
                            </div>
                        )})
                        : ""}
                    </div>
                </div>
            </div>
            <div>
                <div className="info-title">Info</div>
                <div>
                    {selected && Object.values(selected).map((data, i) => {
                        return (
                        <div className="info-option" key={i}>{data}</div>
                    )})}
                </div>
            </div>
        </div>
    )

}

export default ListView;